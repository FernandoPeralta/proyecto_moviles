<!DOCTYPE html>
<html>
<head>
	<title>Lista Marcas</title>
		<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
</head>
<body>
	 	<!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
            	<center>
            	<label>MARCAS</label>
            	<select name="marcas">
            		<option value="0"> SELECCIONE</option>
            		<?php 
            		$sel = $con->prepare("SELECT *from marca");
            		$sel->execute();
               	 	$res = $sel->get_result();
               	 	
               	 	while ($f = mysqli_fetch_array($res)) { 
            			 echo '<option value="'.$f['marca_id'].'">'.$f['marca_nombre'].'</option>';
                        }
                        $sel->close();
                        $con->close();
                    ?>

            	</select>
            	</center>
            </div>
        </div>
<?php
        include 'inc/incluye_datatable_pie.php';
?>
</body>
</html>