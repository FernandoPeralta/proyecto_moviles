<!DOCTYPE html>
<html>
<head>
	<title>Registrar Sucursal</title>
	<meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
</head>
<body>
	<!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
            <form role="form" id="login-form" method="post" class="form-signin" action="sucursal_guardar.php">
            	 <?php
                $sel = $con->prepare("SELECT *from sucursal_prov");
                $sel->execute();
                $res = $sel->get_result();
                ?>

                <div class="h1">Registrar una Sucursal</div>
                <div class="h2">DATOS DE LA SUCURSAL</div>

<div class="form-group">
            <div class="dropdown">
                
               <label>Seleccione un proveedor (requerido)</label>
               <br>
               <button class="btn btn-primary dropdown-toggle" class="caret" type="button" id="dropdownMenuButton" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true" required>
                <font color="black"> 
               <select name="proveedor" required>
                    <option value="0" class="form-control" id="proveedor"> SELECCIONE</option>
                    <?php 
                    $sel = $con->prepare("SELECT *from proveedor");
                    $sel->execute();
                    $res = $sel->get_result();
                    
                    while ($f = mysqli_fetch_array($res)) { 
                         echo '<option value="'.$f['proveedor_id'].'">'.$f['proveedor_nombre'].'</option>';
                        }
                        $sel->close();
                        $con->close();
                    ?>
                      </select>
                      </font>   
               </button>
                </div>
            </div>

                <div class="form-group">
                        <label>Ingresar Nombre de la Sucursal (requerido)</label>
                         <input type="text" name="nombre_sucursal" class="form-control" id="nombre_sucursal" placeholder="Ingrese nombre de la sucursal" style="text-transform:uppercase;" required>
                </div>
                <div class="form-group">
                        <label>Ingresar Direcci&oacute;n</label>
                         <input type="text" name="direccion_matriz" class="form-control" id="direccion_matriz" placeholder="Ingresa dirección (Tienda matriz)" style="text-transform:uppercase;">
                </div>

                <div class="form-group">
                    <label>Tel&eacute;fono 1</label>
                        <input type="text" name="tel_1" class="form-control" id="tel_1" placeholder="Ingresa primer telefono" style="text-transform:uppercase;">

                 </div>

                 <div class="form-group">
                    <label>Teléfono 2</label>
                        <input type="text" name="tel_2" class="form-control" id="tel_2" placeholder="Ingresa segundo telefono" style="text-transform:uppercase;">
                 </div>

                <div class="form-group">
                    <label for="correo_sucursal">Correo Electr&oacute;nico</label>
                        <input type="email" class="form-control" id="correo_sucursal" name="correo_sucursal"
                               placeholder="Ingresa correo electr&oacute;nico" style="text-transform:uppercase;">
                 </div>
                 <br>
                 <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
            </form>
            </div>
        </div>
</body>
</html>