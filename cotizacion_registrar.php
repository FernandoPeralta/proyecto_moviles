<?php
$id_refaccion_seleccionada = $_GET['refaccion_id'];
$nombre_refaccion_seleccionada = $_GET['refaccion_nombre'];
?>
<!DOCTYPE html>
<html>
<head>
	<title>Registrar Cotización</title>
	<meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
</head>
<body>
	 <!--código que incluye el menú responsivo-->
        <?php include'inc/incluye_menu.php' ?>
        <!--termina código que incluye el menú responsivo-->
        <div class="container">
            <div class="jumbotron">
                <form role="form" id="login-form" method="post" class="form-signin" action="cotizacion_guardar.php">
                    <div class="h2">
                        Cotizar una Refacción
                    </div>

                    <div class="h3">
                        Datos de la Refacción
                    </div>
                    <div class="form-group">
                        <label>ID de la refacci&oacute;n seleccionada (<?php echo $nombre_refaccion_seleccionada ?>)</label>
                        <input type="text" id="refaccion_id" class="form-control" name="refaccion_id" value="<?php echo $id_refaccion_seleccionada ?>" readonly="" placeholder="<?php echo $id_refaccion_seleccionada ?>">
                    </div>


                    <div class="form-group">
                        <select name="proveedor" class="form-select form-select-lg mb-3" aria-label=".form-select-lg example" >
                            <option id="proveedor"  selected > SELECCIONE</option>
                                <?php 
                                $sel = $con->prepare("SELECT *from proveedor");
                                $sel->execute();
                                $res = $sel->get_result();
                                
                                while ($f = mysqli_fetch_array($res)) { 
                                     echo '<option value="'.$f['proveedor_id'].'">'.$f['proveedor_nombre'].'</option>';
                                    }
                                    $sel->close();
                                    $con->close();
                                ?>
                            </select>
                            
                    </div>

                    <div class="form-group">
                        <label>Fecha de solicitud de precio(requerido)</label>
                        <input type="date" class="form-control" id="fecha_solicitud" name="fecha_solicitud" step="1" value="<?php echo date("y-m-d");?>"  placeholder="Selecciona la fecha de solicitud" required >
                               
                    <div class="form-group">
                        <label >Precio $ (requerido)</label>
                        <input type="number" class="form-control" id="precio" name="precio" step="0.01" placeholder="precio actual" style="text-transform:uppercase;" required>
                    </div>
                    <br>
                    <button type="submit" class="btn btn-primary">Guardar</button>
                    <input type="reset" class="btn btn-default" value="Limpiar">
                </form>
            </div>
        </div>




</body>
</html>