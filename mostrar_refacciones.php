<!DOCTYPE html>
<html>
<head>
	<title>Mostrar Refacciones</title>
	 <meta charset="UTF-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!--código que incluye Bootstrap-->
        <?php
        include'inc/incluye_bootstrap.php';
        include 'inc/conexion.php';
        include 'inc/incluye_datatable_head.php';
        ?>
    </head>
<body>
    <?php include'inc/incluye_menu.php' ?>
	 <div class="container">
            <div class="jumbotron">
                <?php
                $sel = $con->prepare("SELECT *from refaccion");
                $sel->execute();
                $res = $sel->get_result();
                ?>
                <div class="h2">
                    Selecciona una refacción para agregarle un nuevo precio
                </div>
                <div class="h3">
                    Para agregar una cotización, primero se debe a&ntilde;adir a la base de datos
                </div>
                <div class="h4">
                    1.- Selecciona el nombre de la refacci&oacute;n
                </div>
                <table id="example" class="table table-striped table-bordered" cellspacing="0" width="100%">
                    <thead>
                    <th>ID REFACCIÓN</th>
                    <th>MARCA</th>
                    <th>NOMBRE DE LA REFACCIÓN</th>
                    <th>DESCRIPCIÓN DE LA REFACCIÓN</th>
                    <th>CLIC PARA SELECCIONAR</th>



                    </thead>
                    <tfoot>
                    <th>ID REFACCIÓN</th>
                    <th>MARCA</th>
                    <th>NOMBRE DE LA REFACCIÓN</th>
                    <th>DESCRIPCIÓN DE LA REFACCIÓN</th>
                    <th>CLIC PARA SELECCIONAR</th>

                    </tfoot>
                    <tbody>
                        <?php while ($f = $res->fetch_assoc()) { ?>
                            <tr>
                                <td>
                                    <?php echo $f['refaccion_id'] ?>
                                </td>
                                <td>
                                    <?php echo $f['marca_id'] ?>

                                </td>
                                <td>
                                 <a href="cotizacion_registrar.php?refaccion_id=<?php echo $f['refaccion_id']?>&refaccion_nombre=<?php echo $f['refaccion_nombre'] ?>"><?php echo $f['refaccion_nombre'] ?></a>
                                </td>

                                <td>
                                   <?php echo $f['refaccion_descripcion'] ?>
                                <td>
                                <a href="cotizacion_registrar.php?refaccion_id=<?php echo $f['refaccion_id']?>&refaccion_nombre=<?php echo $f['refaccion_nombre'] ?>"><?php echo $f['refaccion_nombre'] ?>  <img class="img-responsive" src="<?php echo $f['refaccion_imagen'] ?>"/> </a>
                                   
                                </td>
                                </td>







                            </tr>
                            <?php
                        }
                        $sel->close();
                        $con->close();
                        ?>
                    <tbody>
                </table>
               </div>
            </div>
  <?php include'inc/incluye_datatable_pie.php' ?>
</body>
</html>