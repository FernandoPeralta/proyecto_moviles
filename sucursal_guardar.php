<?php
    include 'inc/conexion.php';
    if ($_SERVER['REQUEST_METHOD']== 'POST') {
        $id_proveedor=strtoupper($_POST['proveedor']);
        $nombre_sucursal=strtoupper($_POST['nombre_sucursal']);
        $direccion_matriz=strtoupper($_POST['direccion_matriz']);
        $tel_1=strtoupper($_POST['tel_1']);
        $tel_2=strtoupper($_POST['tel_2']);
        $correo_sucursal=strtoupper($_POST['correo_sucursal']);

try {
   $ins=$con->prepare("INSERT INTO sucursal_prov VALUES(?,?,?,?,?,?,?)");
    $ins->bind_param("issssss",$id,$id_proveedor,$nombre_sucursal,$direccion_matriz,$tel_1,$tel_2,$correo_sucursal);
    if($ins->execute()){
        header("Location: alerta.php?tipo=exito&operacion=Sucursal Guardada&destino=sucursal_agregar.php");
    }
    else{
       header("Location: alerta.php?tipo=fracaso&operacion=Sucursal No Guardada&destino=sucursal_agregar.php");
    }
    $ins->close();
    $con->close();
     
} catch (Exception $e) {
    $e->getMessage(header("Location: alerta.php?tipo=fracaso&operacion=Sucursal No Guardada&destino=sucursal_agregar.php"));
}
    }

    ?>